# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require pypi github [ user=docker project=compose ]
require setup-py [ import=setuptools has_bin=true work=compose-${PV} ]

SUMMARY="Multi-container orchestration for Docker"
HOMEPAGE="https://www.docker.com/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    build+run:
        dev-python/PySocks[>=1.5.6&<2][python_abis:*(-)?]
        dev-python/PyYAML[>=3.10&<4.3][python_abis:*(-)?]
        dev-python/cached-property[>=1.2.0&<2][python_abis:*(-)?]
        dev-python/docker-py[>=3.7.3&<4.0][python_abis:*(-)?]
        dev-python/dockerpty[>=0.4.1&<0.5][python_abis:*(-)?]
        dev-python/docopt[>=0.6.1&<0.7][python_abis:*(-)?]
        dev-python/jsonschema[>=2.5.1&<3][python_abis:*(-)?]
        dev-python/requests[>=2.6.1&<2.21][python_abis:*(-)?]
        dev-python/six[>=1.3.0&<2][python_abis:*(-)?]
        dev-python/texttable[>=0.9.0&<0.10][python_abis:*(-)?]
        dev-python/websocket-client[>=0.56.0&<1.0][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/backports-ssl_match_hostname[>=3.5.0.1][python_abis:2.7]
            dev-python/enum34[>=1.0.4&<2][python_abis:2.7]
            dev-python/functools32[>=3.2.3&<4][python_abis:2.7]
            dev-python/ipaddress[>=1.0.16][python_abis:2.7]
        )
    run:
        app-virtualization/moby
    test:
        python_abis:2.7? ( dev-python/mock[>=1.0.1][python_abis:2.7] )
"

# need a running docker instance
RESTRICT="test"

